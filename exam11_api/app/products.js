const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');

const Product = require('../models/Product');
const User = require('../models/User');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
  Product.find().populate('category', '_id title')
    .then(products => res.send(products))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
  Product.findById(req.params.id).populate('category').populate('user')
    .then(product => {
      if (product) res.send(product);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/',auth, upload.single('image'), (req, res) => {
  const productData = req.body;
  productData.user = req.user._id;
  if  (!(productData.description || productData.image)){
    return res.send.status(400)
  }
  if (req.file) {
    productData.image = req.file.filename;
  }

  const product = new Product(productData);

  product.save(product)
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error))

});

router.delete('/:id',auth,async (req, res) => {
  const success = {message:'Logged out'};
  try {
    console.log(req.params.id);
    const product = await Product.findById(req.params.id);
    if (product.user.equals(req.user._id)){
        await product.remove();
        res.sendStatus(200)
      } else {
        res.sendStatus(401);
      }
        }catch(e)
    {
      res.sendStatus(400)
    }




  return res.send(success)

});



module.exports = router;
