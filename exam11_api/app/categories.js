const express = require('express');
const Category = require('../models/Category');
const Product = require('../models/Product');

const router = express.Router();

router.get('/', (req, res) => {
  Category.find()
    .then(categories => res.send(categories))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);
    const products = await Product.find({category:req.params.id});
    res.send({category, products});
  }catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
