const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Category = require('./models/Category');
const Product = require('./models/Product');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            username: 'demon',
            password: '1',
            displayName: 'Demon',
            phoneNumber: '0700123456',
            token: '1',
        },
        {
            username: 'stary',
            password: '2',
            displayName: 'Старый',
            phoneNumber: '0700221133',
            token: '2',
        },
        {
            username: 'coder',
            password: '3',
            displayName: 'Coder',
            phoneNumber: '0700555666',
            token: '3',
        },
    );
    const category = await Category.create(
        {
            title: 'Бытовая техника',
            description: 'Бытовая техника — электрические механические приборы,' +
                ' которые выполняют некоторые бытовые функции[1], ' +
                'такие как приготовление пищи или чистка. '
        },
        {
            title: 'Сувениры',
            description: 'Сувенирная и раритетная продукция на любой вкус'
        },
        {
            title: 'Продуктовый отдел',
            description: 'Разнообразные продукты питания в ассортименте'
        },
    );
    await Product.create(
        {
            title: 'Холодильник',
            price: '20000',
            description: 'Прибор для замарозки и хранения тпродуктов',
            image: 'holod.jpg',
            category: category[0]._id,
            user: user[0]._id,
        },
        {
            title: 'Газовая плита',
            price: '10000',
            description: 'Прибор для приготовление пищи на открытом огне',
            image: 'plita.jpeg',
            category: category[0]._id,
            user: user[1]._id,
        },
        {
            title: 'Микроволновая печь',
            price: '5000',
            description: 'Прибор для разагревания и приготовления пищи с использованием СВЧ',
            image: 'micro.jpeg',
            category: category[0]._id,
            user: user[0]._id,
        },


        {
            title: 'Кортик разведчика',
            price: '20000',
            description: 'Предмет старины дотируется примерно 40ми годами прошлого века',
            image: 'cortic.jpeg',
            category: category[1]._id,
            user: user[2]._id,
        },
        {
            title: 'Часы командирские',
            price: '7000',
            description: 'часы командирские дата изготовления 1976г',
            image: 'wotch.jpeg',
            category: category[1]._id,
            user: user[2]._id,
        },
        {
            title: 'Сабля',
            price: '50000',
            description: 'Придмет старины ,сабля 18 века',
            image: 'sabl.jpeg',
            category: category[1]._id,
            user: user[2]._id,
        },


        {
            title: 'Хлеб',
            price: '15',
            description: 'Хлеб всему голова',
            image: 'hleb.jpeg',
            category: category[2]._id,
            user: user[1]._id,
        },
        {
            title: 'Мясо',
            price: '300',
            description: 'Мясо свинина свежая',
            image: 'svinina.jpeg',
            category: category[2]._id,
            user: user[1]._id,
        },
        {
            title: 'Кортошка',
            price: '30',
            description: 'Самый необходимый и популярный продукт ',
            image: 'cortoha.jpeg',
            category: category[2]._id,
            user: user[1]._id,
        },

    );

    await connection.close();
};



run().catch(error => {
    console.log('Something went wrong', error);
});