import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {fetchProducts} from "../../store/actions/productsActions";
import ProductListItem from "../../components/ProductListItem/ProductListItem";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {Card, CardBody,NavItem, NavLink} from "reactstrap";
import CardTitle from "reactstrap/es/CardTitle";
import {NavLink as RouterNavLink} from "react-router-dom";



class Products extends Component {
    componentDidMount() {
        this.props.onFetchProducts();
        this.props.onFetchCategories();
    }

    render() {
        return (
            <Fragment>
                {this.props.categories.map(category =>{
                    return  <NavItem key={category._id}>
                                <NavLink tag={RouterNavLink} to={`/categories/${category._id}`} exact>
                                    <Card style={{marginTop: '10px',width:'150px'}}>
                                        <CardBody>
                                            <CardTitle><b>{category.title}</b></CardTitle>
                                        </CardBody>
                                    </Card>
                                </NavLink>
                            </NavItem>
                })}
                {this.props.products.map(product => (
                    <ProductListItem
                        key={product._id}
                        _id={product._id}
                        title={product.title}
                        price={product.price}
                        description={product.description}
                        image={product.image}

                        user={product.user}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    products: state.products.products,
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    onFetchProducts: () => dispatch(fetchProducts()),
    onFetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);
