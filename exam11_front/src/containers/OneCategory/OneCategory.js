import React, {Component, Fragment} from 'react';

import {connect} from "react-redux";

import {fetchOneCategory} from "../../store/actions/categoriesActions";
import {Card, CardBody} from "reactstrap";
import {apiURL} from "../../constants";
import {Link} from "react-router-dom";




class OneCategory extends Component {
    componentDidMount() {
        this.props.onFetchOneCategory(this.props.match.params.id);
    }

    render() {
        console.log("категория", this.props.oneCategory);
        return (
            <Fragment>
                {this.props.oneCategory && this.props.oneCategory.products.map(product =>{
                    return <Card key={product._id} style={{marginBottom: '10px'}}>
                                <CardBody>
                                    <img style={{width: '100px',height: '100px',marginRight: '10px'}} src={apiURL + '/uploads/' + product.image} alt="Card image cap" />

                                    <Link to={'/products/' + product._id}>
                                        {product.title}
                                    </Link>
                                    <strong style={{marginLeft: '10px'}}>
                                        {product.price} KGS
                                    </strong>
                                </CardBody>
                    </Card>
                })}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    oneCategory: state.oneCategory.oneCategory,
});

const mapDispatchToProps = dispatch => ({
    onFetchOneCategory: id => dispatch(fetchOneCategory(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneCategory);