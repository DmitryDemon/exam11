import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardImg, CardText, CardTitle, Col, FormGroup} from "reactstrap";
import {apiURL} from "../../constants";
import {connect} from "react-redux";
import {deleteProduct, fetchOneProduct} from "../../store/actions/productsActions";
// import {fetchOnePost} from "../../store/actions/onePostActions";
// import Comments from "../../components/Comments/Comments";
// import {createComment} from "../../store/actions/commentsActions";

class OneProduct extends Component {
  state = {
    text: '',
    post: this.props.match.params.id
  };

  componentDidMount() {
    this.props.onFetchProduct(this.props.match.params.id);
  }

  // submitFormHandler = event => {
  //   event.preventDefault();
  //
  //   this.props.onCommentCreated({...this.state}).then(
  //     () => this.setState({text: ''})
  //   );
  // };
  //
  // inputChangeHandler = event => {
  //   this.setState({
  //     [event.target.name]: event.target.value
  //   })
  // };

  render() {
    console.log('oneProduct', this.props.oneProduct);
    console.log('user', this.props.user);
    return (
      <Fragment>
        {this.props.oneProduct ?
            <Card style={{width: '640px', marginBottom: '25px'}} >
              <CardImg top style={{width: '200px',marginRight: '10px'}} src={apiURL + '/uploads/' + this.props.oneProduct.image} alt="post" />
              <CardBody>
                <CardTitle>Название: {this.props.oneProduct.title}</CardTitle>
                <CardText>Описания: {this.props.oneProduct.description}</CardText>
                <CardText>Категория: {this.props.oneProduct.category.title}</CardText>
                <CardText>
                  Данные продавца:
                  имя {this.props.oneProduct.user.displayName}
                  <br/>
                  тел. {this.props.oneProduct.user.phoneNumber}
                </CardText>
                {(this.props.oneProduct.user._id === this.props.user._id) ?
                    <FormGroup row>
                      <Col sm={{offset: 2, size: 10}}>
                        <Button onClick={()=>this.props.onDeleteProduct(this.state.post)} type="submit" color="primary">Delete</Button>
                      </Col>
                </FormGroup>:null}

              </CardBody>
            </Card>
        : null}

      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  oneProduct: state.oneProduct.oneProduct,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchProduct: (id) => dispatch(fetchOneProduct(id)),
  onDeleteProduct: id => dispatch(deleteProduct(id))
  // onCommentCreated: (commentData) => dispatch(createComment(commentData))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneProduct);
