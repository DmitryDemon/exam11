import axios from '../../axios-api';
import {push} from 'connected-react-router';

export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';

export const FETCH_ONE_PRODUCT_SUCCESS = 'FETCH_ONE_PRODUCT_SUCCESS';



export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products});
export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});
export const fetchOneProductSuccess = oneProduct => ({type: FETCH_ONE_PRODUCT_SUCCESS, oneProduct});



export const fetchProducts = () => {
  return dispatch => {
    return axios.get('/products').then(
      response => dispatch(fetchProductsSuccess(response.data))
    );
  };
};

export const fetchOneProduct = id => {
  return (dispatch) => {
    return axios.get('/products/' + id).then(
        response => {
          dispatch(fetchOneProductSuccess(response.data));
        }
    );
  };
};

export const createProduct = productData => {
  return (dispatch,getState) => {
    const token = getState().users.user.token;
    return axios.post('/products', productData,{headers:{'Authorisation':token}}).then(
      () => dispatch(createProductSuccess())
    );
  };
};

export const deleteProduct = productData => {
  return (dispatch,getState) => {
    const token = getState().users.user.token;
    return axios.delete('/products/'+productData,{headers:{'Authorisation':token}}).then(
        () => {
          dispatch(createProductSuccess());
          dispatch(push('/'));
        }
    );
  };
};





