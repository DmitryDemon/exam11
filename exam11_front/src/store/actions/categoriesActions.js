import axios from '../../axios-api';


export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_ONE_CATEGORY_SUCCESS = 'FETCH_ONE_CATEGORY_SUCCESS';

export const fetchOneCategorySuccess = oneCategory => ({type: FETCH_ONE_CATEGORY_SUCCESS, oneCategory});

export const fetchCategoriesSuccess = categories => ({type: FETCH_CATEGORIES_SUCCESS, categories});


export const fetchCategories = () => {
    return dispatch => {
        return axios.get('/categories').then(
            response => dispatch(fetchCategoriesSuccess(response.data))
        );
    };
};


export const fetchOneCategory = id => {
    return (dispatch) => {
        return axios.get('/categories/' + id).then(
            response => {
                dispatch(fetchOneCategorySuccess(response.data));
            }
        );
    };
};