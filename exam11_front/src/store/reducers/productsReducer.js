import {FETCH_ONE_PRODUCT_SUCCESS, FETCH_PRODUCTS_SUCCESS} from "../actions/productsActions";


const initialState = {
  products: [],
  oneProduct: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_SUCCESS:
      return {...state, products: action.products};
    case FETCH_ONE_PRODUCT_SUCCESS:
      return {...state, oneProduct: action.oneProduct};
    default:
      return state;
  }
};

export default reducer;


