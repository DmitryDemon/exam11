import {FETCH_CATEGORIES_SUCCESS, FETCH_ONE_CATEGORY_SUCCESS} from "../actions/categoriesActions";

const initialState = {
    categories: [],
    oneCategory: null,
};

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: action.categories};
        case FETCH_ONE_CATEGORY_SUCCESS:
            return {...state, oneCategory: action.oneCategory};
        default:
            return state;
    }
};

export default categoriesReducer;